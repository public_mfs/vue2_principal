//相当于给属性赋值操作
function def(obj, key, value, enumerable) {
    Object.defineProperty(obj, key, {
        enumerable,
        value,
        writable: true,
        configurable: true
    })
}

export {def}