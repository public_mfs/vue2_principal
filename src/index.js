import observe from './observe';
import Watcher from './Watcher';

let obj = {
    a: {
        m: {
            n: 123
        }
    },
    b: 666,
    g: [1, 3, 4]
}

console.log(`***********observe(obj)`);
observe(obj);
console.log(`***********obj.b++`);
obj.b++;
console.log(`***********obj.a.m.n++`);
obj.a.m.n++;
console.log(`***********obj.g.push(322)`);
obj.g.push(322);
console.log(`***********obj.g.splice(0, 1, [999], 888)`);
obj.g.splice(0, 1, [999], 888);
console.log(`***********obj.b = {dd:444}`);
obj.b = { dd: 444 };
console.log(`***********obj.b.dd++`);
obj.b.dd++;
console.log(`***********new Watcher(obj,'a.m.n',()=>{console.log("改了a.m.n了！"+obj.a.m.n)})`);
new Watcher(obj, 'a.m.n', () => { console.log("改了a.m.n了！" + obj.a.m.n) });
console.log(`***********obj.a.m.n++`);
obj.a.m.n++;
console.log(`***********obj.g.push(99)`);
obj.g.push(99);


// // obj.g.splice(0, 1, [999], 888);//push等改写方法中也进行了ob.dep.notify()触发依赖
// // obj.b = {dd:444};//修改为对象也同样会触发响应式，因为set中对new_value也进行了observe
// // obj.b.dd++;
// console.log(obj);
// new Watcher(obj,'a.m.n',()=>{console.log("改了a.m.n了！"+obj.a.m.n)});
// obj.a.m.n++;
// obj.g.push(99);