import { def } from "./untils";

const arrayProto = Array.prototype;
const arrayMethods = Object.create(arrayProto);//以Array的prototype为原型创建一个新对象，因为push等7个方法是定义在Array.prototype上的

let methodsToPatch = [
    'push',
    'pop',
    'shift',
    'unshift',
    'splice',
    'sort',
    'reverse'
]

methodsToPatch.forEach(function (method) {
    const original = arrayProto[method];//备份数组中的原push等操作
    def(arrayMethods, method, function () {
        const result = original.apply(this, arguments);//这里这样写的话就是保持数组原来的push等方法不变了，设计的很巧妙，因为def相当于赋值arrayMethods.push=function，因此arr.push(123)时this指向arr，arguments就是123,且push为未重写过的original push，youyuxi好聪明

        let inserted = [];
        const ob = this.__ob__;
        console.log(arguments);
        const args = [...arguments];
        // const args = Array.from(arguments);

        //对push、unshift、splice新增的数组也要进行observe
        switch (method) {
            case 'push':
            case 'unshift':
                inserted = args;
                break;
            case 'splice':
                inserted = args.slice(2);
        }
        if (inserted) {
            ob.observeArray(inserted);
        }
        ob.dep.notify();
        console.log("数组响应了！");

        return result;//要有返回值，因为pop、shift这些会返回被删掉的元素
    }, false)
})
console.log(arrayMethods);

export { arrayMethods };