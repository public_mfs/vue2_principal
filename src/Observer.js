import { def } from './untils';
import defineReactive from './defineReactive';
import { arrayMethods } from './array';
import observe from './observe';
import Dep from './Dep';

class Observer {
    // 在observe函数中的value没有__ob__才会new一个Observer类，并且需要将这个Observer实例放在value.__ob__中
    constructor(value) {
        console.log("Observer类的构造器");
        
        //**Dep在Observer中实例化，每一个Observer实例都要有一个Dep实例**，用于收集依赖Watcher
        //这里实例化Dep是因为数组变动时触发依赖以及Vue.set和Vue.delete中都要用到
        this.dep=new Dep();

        def(value, '__ob__', this, false);
        if(Array.isArray(value)){
            value.__proto__=arrayMethods;//源码中将这一句封装成了一个protoAugment函数，实现的话也可以使用es6推荐的Object.setPrototypeOf
            this.observeArray(value);//观察数组的方法
        }else{
            this.walk(value);//调用观察对象的方法
        }
    }
    walk(value) {//观察数组的方法：walk内通过defineReactive遍历下一层属性并设置defineReactive
        for (let key in value) {
            defineReactive(value, key);
        }
    }
    observeArray(value){
        for(let i=0,l=value.length;i<l;i++){//这里之所以要用l来放置数组的长度，是为了应对数组在遍历过程中出现长度改变等特殊情况
            observe(value[i]);
        }
    }
}

export default Observer;