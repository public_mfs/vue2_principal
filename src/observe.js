import Observer from './Observer';

function observe(value) {
    if (typeof value != 'object') return;//如果传入的不是一个对象，则什么也不做返回undefined
    let ob;//ob表示value.__ob__这个Observer对象
    if (typeof value.__ob__ != 'undefined') {
        ob = value.__ob__;
    } else {
        ob = new Observer(value);
    }
    return ob;
}

export default observe;