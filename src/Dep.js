let uid = 0;
class Dep {
    constructor() {
        console.log("Dep类构造器");
        this.id = uid++;
        this.subs = [];
    }
    notify() {//通知更新
        console.log("触发notify");
        const subs = this.subs.slice();// stabilize the subscriber list first
        for (let i = 0, l = subs.length; i < l; i++) {//这里使用l保存subs.length避免操作过程中数组元素增加/减少
            subs[i].update();//对watcher列表进行遍历并且执行更新操作
        }
    }
    depend() {//添加依赖
        if (Dep.target) {//Dep.target用来盛放全局的watcher
            this.addSub(Dep.target);
        }
    }
    addSub(sub) {//添加订阅者
        this.subs.push(sub);
    }
}

export default Dep;