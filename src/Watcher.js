import Dep from "./Dep";

let uid = 0;
class Watcher {
    constructor(target, expression, callback) {
        this.id = uid++;
        this.target = target;
        //getter是一个function！！！！
        this.getter = parsePath(expression);//返回的是一个函数function(obj)，这个function会返回obj内expression最终表示的那个对象
        this.callback = callback;
        //手动调用get方法，也就是“touch”一下
        this.value = this.get();
    }
    update() {
        console.log("调用watcher的update方法");
        this.run();
    }
    get() {
        Dep.target = this;//全局的target设置为watcher本身，进入依赖收集阶段
        const target = this.target;
        let value;
        try {
            value = this.getter(target);//得到当前touch的对象
        } catch (e) {

        } finally {
            Dep.target = null;
        }
        return value;
    }
    run() {
        var value = this.get();
        if (value !== this.value || typeof value == 'object') {
            var oldValue = this.value;
            this.value = value;
            this.callback.call(this.target, value, oldValue);
        }
    }
}

function parsePath(path) {
    var segments = path.split('.');
    return function (obj) {
        for (var i = 0; i < segments.length; i++) {
            if (!obj) { return }
            obj = obj[segments[i]];
        }
        return obj
    }
}
export default Watcher;