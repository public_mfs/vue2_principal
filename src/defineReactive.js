import observe from "./observe";
import Dep from "./Dep";

function defineReactive(data, key, val) {//val相当于一个初始值，并且作为闭包临时变量的容器
    console.log(`defineReactive被调用！data:${JSON.stringify(data)}    key:${key}   val:${val}`);
    const dep = new Dep();//形成闭包，使得每个数据都维护一个属于自己的watcher数组
    if (arguments.length == 2) {
        val = data[key];//这里是直接将data.key设置为响应式，但是不为其赋初值的情形，比如将现有的key设为响应式就不需要传val
    }

    let childOb = observe(val);//子属性/子元素也要observe，多个函数进行循环

    Object.defineProperty(data, key, {
        get() {
            // console.log(`你正试图访问${key}属性`);
            if(Dep.target){
                dep.depend(Dep.target);
                if(childOb){
                    childOb.dep.depend();
                }
            }
            return val;
        },
        set(new_value) {
            if (new_value === val) {
                return;
            }
            console.log(`试图修改${key}属性为${new_value}`);
            val = new_value;

            childOb = observe(new_value); //设置了新值，则也要对new_value进行observe
            dep.notify();//发布订阅模式，去通知每一个watcher
        },
        enumerable: true,//可枚举，可以被for in或者Object.keys遍历
        configurable: true//可配置，比如可以被delete
    })
}

export default defineReactive;