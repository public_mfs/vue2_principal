# 目录
[TOC]

## 响应式原理
### MVVM
1. 数据变化，视图也会自动变化
2. Vue的数据变化是非侵入性的：不需要调用其他任何API（比如react的 `this.setState`或者小程序的 `this.setData` ：为侵入式的），而是直接操作data数据
3. 为什么说Vue数据变化时非侵入式的呢？因为他的data的property被Object.defineProperty劫持了setter/getter，只要直接执行data.property独写操作就能够触发对应的getter/setter函数

### Object.defineProperty()
#### 用法：
1. 
```javascript
Object.defineProperty(
    obj,
    'prop_name',
    {
        //value: undefined, //设置属性值，和get互斥，一起设置会报错
        //writable: false, //是否可以被赋值运算符改变，为false改值操作无效
        //configurable: false, //是否能被改变或者该属性能否被删除
        set: undefined,
        get: undefined,
        //enumerable: false, //是否可以被枚举，为true时可以被for(let key in obj)或者Object.keys()遍历
    })
```
2. 设置getter和setter的属性key，进行key++操作时会同时访问getter（先）和setter（后）属性
3. **缺点**：不能够直接在Object.defineProperty的getter/setter中对obj的属性进行读写，因此需要使用一个临时变量来保存这个属性值

### defineReactive函数
使用闭包保存属性值+Object.defineProperty()劫持setter/getter，相当于对Object.defineProperty()进行了一次封装，使用闭包提供了一个临时变量对getter/setter进行周转
```javascript
function defineReactive(data,key,val){//val相当于一个初始值，是闭包中保存变量的中转值    
    Object.defineProperty(data,key,{
        configurable:true,//设置成可配置，比如可以被delete        
        enumerable:true,//可枚举，可以被Object.keys()或者for in遍历        
        get(){            
            console.log("你访问obj的"+key+"属性");            
            return val;        
        },        
        set(new_value){            
            console.log(`你试图修改obj的${key}属性`);            
            if(new_value===val){ return; }            
            val=new_value;       
        }    
    })
}
let obj={};
defineReactive(obj, 'my_key',10);//vue2为对象设置一个响应式属性，初始值为10
```

### 递归侦测对象全部属性
#### Observer类：
1. Observer是什么：将一个正常的obj转化成任何属性（嵌套/非嵌套）都能被侦测到的工具类——**观察者**，也就是每个层级的属性都是响应式的
2.  嵌套对象的内部属性无法通过defineReactive实现响应式，因为Object.defineProperty无法对obj.m.n属性进行劫持的能力：~~Object.defineProperty(obj, 'm.n', {set(){}})~~
#### observe函数
1. observe(value)函数做了什么
     ![b282d41b2e9db834e74bca3f60941d55.png](en-resource://database/1005:1)
 2. 具体实现
    ```mermaid
    graph 
    A[observe方法, 参数是value]
    B[value有没有__ob_, 其实__ob__就是用于存放Observer]
    A-->B
    B--如果没有__ob__属性-->C[new一个Observer添加到value.__ob__上,<br/> 通过在Observer构造函数中调用def函数实现]
    D[Observer内包含一个walk方法: <br/>遍历下一层属性并且逐个设置defineReactive]
    C--value不是数组-->D
    C--value是数组-->G[将value的原型设置成新建的arrayMethods对象,<br/> 对象内重写了push等数组方法]
    E[设置某个属性值的时候, 触发set的new_value也需要observe]
    D-->E-->A
    ```
#### 递归顺序
是循环调用，而不是自己调用自己
```mermaid
graph
F[observe]-->G[Observer类]-->H[defineReactive函数]-->F
```
#### 总结
1. observe(value)，观察一个对象
    * 如果value不是一个对象，直接return
    * 如果`value.__ob__`存在，则返回返回`value.__ob__`
    * 如果`value.__ob__`不存在，则新建一个Observer(value)，并返回该Observer，Observer类中中会包含把自身实例添加到`value.__ob__`上的操作
2. Observer类干了什么
    * 构造函数中通过def函数将this赋值给`value.__ob__`，并且调用`walk(value)`方法
    * `walk(value)`方法：对value的每一个可枚举属性进行遍历，并且进行`defineReactive`
3. defineReactive(data, key, val)
    * 使用闭包对Object.defineProperty进行封装，参数只有2个的时候val直接被赋值为data[key]
    * 对每一个val进行observe观察
    * setter中的`new_value`也要进行observe观察
    
### 数组的响应式原理（在数组3个特殊方法上添加观察操作）
1. 对数组的7个方法进行改写：push、pop、shift（删除）、unshift、splice、sort、reverse，这7个方法（methodsToPatch）都是定义在Array.prototype上。如何改写？
    1. 以Array.prototype为原型创建一个对象，对新创建的arrayMethods对象的上述7中方法进行重写，需要用保存数组原有的push等方法，因为我们只需要在原有的7个方法上添加观察操作
    ```javascript
        var arrayProto = Array.prototype;
        var arrayMethods = Object.create(arrayProto);
        methodsToPatch.forEach(function (method) {
            const original = arrayProto[method];//保存原有的数组操作
            def(arrayMethods, method, function () {
                console.log ( "对methodsToPatch数组的7个方法进行重写的代码" ) ;
                console.log ( "function内的this指向数组本身，this时包含__ob__属性的" ) ;
            })
        })
    ```
    2. 在Observer类的constructor判断value的类型，**修改为如果是对象则仍然执行this.walk(value)，如果是数组则设置 <u>`value.__proto__=arrayMethods`</u>**（或者也可以用es6的 `Object.setPrototypeOf(value, arrayMethods)`）将value的原型设置为新建的arrayMethods对象
    3. **push、unshift、splice**三种方法需要特殊处理，因为可能会造成数组元素增加，需要对新增的元素也进行observe观察（[...arguments].slice()取新添加的元素数组），由于def函数重写push等方法时，function内的this仍然指向数组本身，因此可以直接使用`this.__ob__.observeArray`进行新增元素数组的观察
    4. 重写function的结尾记得要返回 original.apply(this, arguments)的结果

### 依赖收集（用到数据的地方--组件称为依赖）
![e3607782905e6497254da76550a8d0de.png](en-resource://database/1010:1)

#### getter中收集依赖，在setter中触发依赖
#### Dep类和Watcher类
**依赖收集的代码封装成Dep类，用来管理依赖**
##### Dep在哪儿实例化
1. **每个Observer实例中都有一个Dep实例**：在Observer构造函数中实例化  `this.dep=new Dep()` 
    * Observer中实例化一个Dep是因为数组3个方法的重写需要使用ob.dep.notify()进行发布（因为数组不能通过Object.defineProperty进行劫持，对push等方法进行劫持并notify相当于setter中触发依赖），并且Vue.$set、Vue.$delete也会用到；
2. **defineReactive函数中也有一个Dep实例**
    使用闭包存储每个属性的watcher依赖数组
##### dep.notify以及dep.depend在哪调用
* dep.notify（4个地方调用） ：
    1. defineProperty(data, key, val)的**setter**中 `dep.notify()`
    2. 数组3个方法改写时：`ob.dep.notify()`
    3. Vue.$set以及Vue.$delete中：`ob.dep.notify();`
* dep.depend：
    1. defineProperty(data, key, val)的**getter**中 `dep.depend()`
    2. defineProperty的getter中如果子元素是一个对象 `childOb.dep.depend();`

    




3. Watcher是一个中介，保存了所watch对象的回调函数，每当数据发生变化时通知组件的Render Function更新组件
3. Dep收集Watcher
4. Dep使用发布订阅模式，当数据发生变化时，会循环依赖列表，把所有的Watcher都通知一遍，每个Watcher进行update
5. vue的巧妙之处在于：Watcher把自己设置到一个全局的指定位置，然后读取（touch）→触发getter→getter中得到当前读取数据的Watcher，并把Watcher收集到Dep中

#### Dep类实现


## 虚拟Dom和diff算法
 jq时代：DOM操作 → 视图更新
vue时代：**数据改变 → 虚拟DOM（计算）→ 操作真实的DOM → 视图更新虚拟DOM就是通过js模拟dom结构**

### 虚拟Dom：使用JS表达Dom结构
1. V8引擎执行JS速度比直接操作Dom要快很多，因此使用类似于AST抽象语法树的概念将真实的Dom结构转化成一个个JS对象，使用JS表达Dom结构
    * **tag**：标签名为对象的tag属性
    * **props**：标签的style、class等转化成props属性内的style、className等子属性
    * **children**：标签的子标签数组
    * ...
2. 虚拟Dom先计算完成之后，再一次性更新实际Dom结构（以最小代价--diff算法--来实现Dom更新，提升性能）
3. 为什么要使用虚拟Dom？
    1. 数据驱动视图，Dom操作交给框架来操作
    2. 高效地控制Dom操作（diff算法）

### 虚拟Dom核心：diff算法
```javascript
//以下是vue源码的注释，vue的diff算法参考了Snabbdom
  /**
   * Virtual DOM patching algorithm based on Snabbdom by
   * Simon Friis Vindum (@paldepind)
   * Licensed under the MIT License
   * https://github.com/paldepind/snabbdom/blob/master/LICENSE
   *
   * modified by Evan You (@yyx990803)
   *
   * Not type-checking this because this file is perf-critical and the cost
   * of making flow understand it is not worth it.
   */
```
#### 介绍
1. 找出两个对象（把对象看程两棵树）之间的差异，目的是尽可能做到节点复用
2. 传统的diff算法复杂度达到O(n^3)，效率低下
3. vue对diff的优化：优化后的时间复杂度为O(n)
    1. 只比较同一个层级，不会跨级比较
    2. 标签名不同，直接删除，不继续在该节点往下进行深度比较
    3. 标签名相同且key值相同的时候，不会继续进行深度比较

#### 具体实现
##### 1. vnode（虚拟节点）结构：真实Dom的AST
```javascript
var VNode = function VNode (
    tag, // 标签名称
    data, // 标签属性（事件、属性等）
    children, //子元素
    text, //children和text只会有一个，必有一个为undefined，也就是说标签内要么就是子元素数组，要么就是字符串
    elm, // 对应的真是Dom元素
    context, 
    componentOptions,
    asyncFactory
  )
```
##### 2. 实现一个patch函数
1. patch ( oldVnode: Vnode | Element , vnode: Vnode)使用场景：
    * 会在**首次页面渲染**的时候执行一次，将vnode渲染到真实Dom（对应Element）上；
    * 其次`patch(vnode, new_vnode)`会将新的vnode替换掉老的vnode；
2. patch实现流程：
```mermaid
graph
A[patch函数接收oldVnode和vnode两个参数, 判断oldVnode是否为vnode类型]
A--不是-->B[oldVnode=创建一个空的vnode并且关联Dom]
C[通过sameVnode函数判断oldVnode和vnode是否为相同的Vnode  比较key/tag/等]
A--是-->C
B-->C
C--是-->D[patchVnode]
C--否-->E[创建新的Dom元素, 插入新的Dom元素并且移除老的Dom元素]
```
#### 3. patchVnode（包含大量的if else运算）
1. 比较oldVnode和vnode
2. 如果相同，直接返回
3. 如果不同，一切以新的vnode为准
4. 新vnode有children，旧的需要删除原来的text，添加新的children，或者更新children（更新children使用updateChildren函数）
5. 新vnode有text，旧的需要删除原来的children，添加新的text，或者更新text
6. PS：children 和 text 是互斥的，不会同时存在。

#### 4. updateChildren函数
1. 判断sameVnode( oldStartVnode, newStartVnode)老的开始虚拟节点和新的开始虚拟节点是否同一个，是同一个的话指针往右移动（*startVnode往右移，*endVnode往左移）
2. 同上继续判断：老结束vs新结束、老开始vs新结束、老结束vs新开始
3. 以上4中sameVnode情况都没有命中，则比较key值
4. 拿到新开始的key，在老children里找对应的key，没有找到则创建新元素；如果老children中找到了对应的key，再对比tag是否相等
5. 如果tag不相等，那么说明仍然不是相同的节点，同样创建新的元素并插入
6. 如果tag相等（key相等+tag相等），和patch函数一样执行patchVnode


## 总结
在Jquery时代我们通过直接操作dom来实现视图的更新，而在Vue时代则是通过mvvm框架来实现视图的更新，数据变化，视图也会自动变化，而这种响应式则是由框架来完成，开发者并不需要关心底层的dom操作。

Vue的数据变化监听和响应是非侵入式的，不需要调用其他任何API（比如react的 this.setState或者小程序的 this.setData ：为侵入式的），而是直接操作data数据就能达到视图更新的效果，而实现这一功能最关键的一步就是使用Object.defineProperty方法，这个方法能够劫持对象某个属性的getter/setter/configurable/enumerable等属性描述符并进行修改，比如我们可以劫持obj.a的getter/setter并在其中添加一些操作，就可以实现对对象某个属性读写的“监听”。
但是Object.defineProperty有一个缺点：需要设置一个全局变量来保存属性值，因此vue中定义了defineReactive函数对这个方法进行闭包地封装，这样对对象的某一个属性的监听就可以调用defineReactive方法。

首先在vue中有一个observe函数，接收一个value参数，observe的功能是给value对象添加一个Observer实例

Observer类用来给对象的属性执行defineReactive。首先，构造函数内会把this赋值给`value.__ob__`，并且对对象和数组进行不同的处理，对对象使用walk函数遍历每一个key并且执行defineReactive，defineReactive内对子属性以及setter中设置的新值进行observe，实现对对象的响应式监听；
对数组需要做特殊处理，因为数组不能通过defineReactive进行响应式监听，我们需要进行遍历并且observe其中的每一个元素，并且，对能够引起数组变化的7个方法（push、pop、shift、unshift、sort、reverse、splice）进行了重写：以Array.prototype为原型创建了一个新的对象，再修改这个新对象的7个方法（说是7个，其实**目前**真正修改的只有3个，也就是能够让数组数组元素增加的push、unshift、splice方法，**在原生方法的基础上添加了对新增元素数组的一个observe**（arguments截取参数获取）），再将这个修改后的对象作为value的原型，达到数组变化的一个响应式监听

所以目前通过observe调用Observer类，Observer类调用defineReactive，defineReactive调用observe函数的一个循环递归来实现对每一层属性的响应式监听

接下来是依赖收集阶段：
代码中用到数据的组件称为依赖，在代码中也就是我们的Watcher类，使用Dep类来管理依赖，Watcher（订阅者）和Dep（发布者）类属于观察者模式（没有事件中心），Dep接受到数据变化时会通知Watcher订阅者执行回调函数（比如视图更新、虚拟Dom diff算法）
对于data的每个属性都需要有自己的发布者，因此在defineReactive中我们会使用闭包保存一个Dep实例，Dep会在getter中收集依赖，将当前被“touch”的依赖添加到该属性的依赖列表中，在setter中触发依赖，遍历通知它的Watcher列表并执行更新视图等回调函数操作，其次，数组变动时也会触发依赖；
除此之外每一个属性上面的Observer中都会包含有一个Dep实例（发布者），因为数组变动时需要通过数组上的Observer实例获取dep来触发依赖，并且`Vue.$set`以及`Vue.$delete`中也会使用到这个dep
Watcher类包含了当前监听属性发生变动时的回调，其get函数内会将this赋值给一个全局变量Dep.target，然后手动调用get方法（构造函数中+update中），（尤雨溪将其形容成一次touch），这样，Dep在Object.defineProperty的getter中将依赖添加到其依赖列表中
getter中收集依赖，setter中触发依赖，由此实现数据和视图的响应式更新

