const path = require('path');
const html_webpack_plugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    devServer: {
        port: 8888,
        hot: true,
        static: './dist'
    },
    devtool:'inline-source-map',
    plugins: [
        new html_webpack_plugin({
            title: "自动生成html文件"
        }),
        new CleanWebpackPlugin(),
        new webpack.HotModuleReplacementPlugin()
    ]

}